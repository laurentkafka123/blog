import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.css']
})
export class PostListComponentComponent {

  @Input() posttitle: string;
  @Input() postcontent: string;
  @Input() postloveit: number;
  @Input() postcreate_at: string;
  constructor() { }
  onLoveIt() {
    this.postloveit++;
    console.log('love it selected ' + this.postloveit );
  }

  onDontLoveIt() {
    this.postloveit--;
    console.log('don\'t love it selected ' + this.postloveit );
  }
  isLessThan() {
    if (this.postloveit < 0) {
      return true;
    } else {
      return false;
    }
  }
  isMoreThan() {
    if (this.postloveit > 0) {
      return true;
    } else {
      return false;
    }
  }
}
