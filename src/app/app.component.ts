import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  posts = [
   {
      title : 'Mon premier post',
      content : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, animi architecto aut, debitis dignissimos eius',
      loveIts : 1,
      created_at : new Date()
    },
   {
      title : 'Mon dexieme post',
      content : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, animi architecto aut, debitis dignissimos eius',
      loveIts : -1,
      created_at : new Date()
    },
   {
      title : 'Encore un post',
      content : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, animi architecto aut, debitis dignissimos eius',
      loveIts : 0,
      created_at : new Date()
    }
  ];
  onPostLehgth(){
    return this.posts.length;
  }
}
